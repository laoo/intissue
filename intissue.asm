    icl 'lnxheader.asm'

v0  equ 0

    ;some mandatory initializations

	lda #LYNX.@DISPCTL(DISP_COLOR|DISP_FOURBIT|DMA_ENABLE)
	sta LYNX.DISPCTL
	lda #LYNX.@SERCTL(TXOPEN)
	sta LYNX.SERCTL
	lda #LYNX.@IO(READ_ENABLE|RESTLESS|CART_POWER_OFF)
	sta LYNX.IODIR
	lda #LYNX.@IO(RESTLESS|CART_ADDR_DATA)
	sta LYNX.IODAT
	lda #$ff
	sta LYNX.MSTEREO
	stz LYNX.MPAN
	stz LYNX.AUDIO3_VOLCNTRL
	stz LYNX.AUDIO2_VOLCNTRL
	stz LYNX.AUDIO1_VOLCNTRL
	stz LYNX.AUDIO0_VOLCNTRL
	lda #LYNX.@SUZYBUSEN(ENABLE)
	sta LYNX.SUZYBUSEN
	lda #$F3
	sta LYNX.SPRINIT
	lda #LYNX.@SPRSYS(NO_COLLIDE|UNSAFEACCESSRST)
	sta LYNX.SPRSYS
	stz LYNX.HOFF
	stz LYNX.HOFF+1
	stz LYNX.VOFF
	stz LYNX.VOFF+1
	lda #$7F
	sta LYNX.HSIZOFF
	sta LYNX.VSIZOFF
	stz LYNX.SDONEACK
	lda #2
	sta LYNX.COLLOFF
  
	lda #$ff
	sta LYNX.INTRST
	lda #LYNX.@MAPCTL(VECTOR_SPACE)
	sta LYNX.MAPCTL
	lda #<IRQ
	sta LYNX.CPU_IRQ
	lda #>IRQ
	sta LYNX.CPU_IRQ+1

    ;video timers
	lda #LYNX.@TIM_CONTROLA(ENABLE_RELOAD|ENABLE_COUNT)
	sta LYNX.HCOUNT_CONTROLA
	;hardcoded refresh rate for 78.7 Hz
	lda #120
	sta LYNX.HCOUNT_BACKUP
	lda #LYNX.@TIM_CONTROLA(ENABLE_INT|ENABLE_RELOAD|ENABLE_COUNT|AUD_LINKING)
	sta LYNX.VCOUNT_CONTROLA
	lda #104 ;backup value for vertical scan timer (== 102 vertical lines plus 2)
	sta LYNX.VCOUNT_BACKUP
	lda #31
	sta LYNX.PBKUP

    ;serial timer
	lda #LYNX.@TIM_CONTROLA(ENABLE_RELOAD|ENABLE_COUNT|AUD_1)
	sta LYNX.SERIALRATE_CONTROLA
	lda #1
	sta LYNX.SERIALRATE_BACKUP
	lda #LYNX.@SERCTL(RXINTEN|PAREN|RESETERR|TXOPEN)
	sta LYNX.SERCTL

    ;video to $c000, clearing memory
	stz LYNX.DISPADR
    lda #$c0
	sta LYNX.DISPADR+1
    sta v0+1
    lda #0
    sta v0
    tay

@   sta (v0),y
    iny
    bne @-
    inc v0+1
    ldx v0+1
    cpx #$c0+32
    bcc @-

    ;enabling interrupts and...
	cli

    ;looping with clearing the background color
loop
    stz LYNX.GREEN0
    stz LYNX.BLUERED0
    bra loop

IRQ
    ;no need to preserve registers as main loop does not use them
	lda LYNX.INTSET
	and #LYNX.@INT(SERIAL)
	beq vbi ;checking for VBL interrupt

serial
    sta LYNX.INTRST         ;resetting interrupt
    lda LYNX.SERCTL
    and #LYNX.@SERCTL(RXRDY)
    beq @+
    lda LYNX.SERDAT         ;reading data hence acking the reception
    ldx idx
    inc idx                 ;setting background color to different value each interrupt
    lda color,x
    sta LYNX.BLUERED0
    rti
@   lda #$0f
    sta LYNX.GREEN0
    rti

idx     .he 00
color   .he 0f f0 ff    ;only first two colors are used hence the interrut is triggered twice

vbi
    lda #LYNX.@INT(VERTICAL)
    sta LYNX.INTRST         ;resetting vbi

    ;delay to position the issue
    ldy #4
    ldx #0
@   dex
    bne @-
    dey
    bne @-

    stz idx                 ;resetting color index
    lda #{bit #}
    ldx LYNX.JOYSTICK
    beq @+
    lda #$08
    sta LYNX.SERDAT         ;sending byte only when any joystick button is pressed
@   rti

    ;icl 'footer.asm'
    icl 'lynxhard.asm'
