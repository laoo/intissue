	opt c+f+h-

    org [b(_end-_start)],$00

encrypted_block_size = 52
header_size_read_by_loader = 1

cursor_offset = encrypted_block_size + header_size_read_by_loader + ( _end-_start )

_start
	clc
	ldx #cursor_offset
loop1
	ldy #4
loop2
	lda $fcb2
ptr = *+1
	sta $200
	lda ptr
	adc #1
	sta ptr
	lda ptr+1
	adc #0
	sta ptr+1
	cmp #$c0	;reading data from cart upto this address
	bcc @+
	jmp $200
@	inx
	bne loop2
	dey
	bne loop2

	inc sector
sector equ *+1
	lda #0
	jsr $fe00
	bra loop1
_end
